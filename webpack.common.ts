import path from 'path';
import webpack from 'webpack';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import fs from 'fs';
// @ts-ignore
import { gitDescribeSync } from 'git-describe';

const exec = require('child_process').exec;

const environmentResolver = (): string => {
  for (let i = 0; i < process.argv.length; i++) {
    const arg = process.argv[i];
    if (arg === '--env') {
      if (i + 1 < process.argv.length) {
        const flagValue = process.argv[i + 1];
        switch (flagValue) {
          case 'dev':
            return 'environment.dev.ts';
          case 'qa':
            return 'environment.qa.ts';
          case 'prod':
            return 'environment.prod.ts';
          default:
            throw new Error(`Unknown environment "${flagValue}". Available environments: dev, qa, prod`);
        }
      } else {
        throw new Error('Missing environment');
      }
    }
  }
  return 'environment.ts';
};

const config: webpack.Configuration = {
  module: {
    rules: [
      {
        exclude: [path.resolve(__dirname, 'node_modules')],
        test: /\.ts$/,
        use: 'ts-loader'
      },
      // {
      //   exclude: /node_modules/,
      //   test: /\.graphql$/,
      //   use: [{ loader: 'graphql-import-loader' }]
      // },
      {
        exclude: /node_modules/,
        test: /\.graphql?$/,
        use: [{ loader: 'webpack-graphql-loader' }]
      }
    ]
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })]
  },
  plugins: [
    new webpack.NormalModuleReplacementPlugin(
      /src\/config\/environment\/environment\.ts/,
      `./${environmentResolver()}`
    ),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
      reportFilename: '../reports/api-report.html'
    }),
    new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }]),
    {
      apply: (compiler: webpack.Compiler): void => {
        compiler.hooks.afterPlugins.tap('Generate git-version.json file', (): void => {
          const gitInfo = gitDescribeSync(__dirname);
          fs.writeFile(`./git-version.json`, JSON.stringify(gitInfo), 'utf8', () =>
            // tslint:disable-next-line:ban
            console.log('"git-version.json" created!')
          );
        });
      }
    }
  ],
  target: 'node',
  node: {
    __dirname: false
  }
};

export default config;
