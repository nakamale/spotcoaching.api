# SAP-CAI API

## Skeleton for Node.js application written in TypeScript

the fallowing npm-scripts are supported:

### Development

```bash
npm run dev
```

### Linting

```bash
npm run lint
```

### Building

```bash
npm run build
```

### Start

```bash
npm start
```

## Setup Test-Server (autom. restart)

- start local server

```bash
ngrok http 8000
```

- copy ngrok-url into cai-settings
- start app

```bash
npm run dev
```
