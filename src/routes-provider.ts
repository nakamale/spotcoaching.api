import express from 'express';

export abstract class RoutesProvider {
  protected readonly router: express.Router;

  protected constructor() {
    this.router = express.Router();
    this.mountRoutes();
  }

  protected abstract mountRoutes(): void;

  public getRouter(): express.Router {
    return this.router;
  }
}
