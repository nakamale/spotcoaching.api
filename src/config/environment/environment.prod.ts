import { EnvironmentDefinition, Environments } from '@environment/environment-definition';

export const environment: EnvironmentDefinition = {
  environment: Environments.PROD,
  url: 'https://api.spotcoaching.rhyno.ch/',
  port: 8000,
  firebase: {
    credential: require('../ccredentials'),
    dbUrl: 'TODO',
    projectId: 'spotcoaching-dev',
    storageBucket: 'TODO'
  },
};
