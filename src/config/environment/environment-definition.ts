import { ServiceAccount } from 'firebase-admin';

export interface EnvironmentDefinition {
  environment: Environments;
  url: string;
  port: number;
  firebase: {
    credential: ServiceAccount;
    dbUrl: string;
    projectId: string;
    storageBucket: string;
  };
}

export enum Environments {
  PROD = 'PROD',
  DEV = 'DEV',
  LOCAL = 'LOCAL'
}
