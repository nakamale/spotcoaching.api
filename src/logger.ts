import { environment } from '@environment/environment';
import { Environments } from '@environment/environment-definition';
import { format, TransformableInfo } from 'logform';
import * as winston from 'winston';
import { loggers, transports } from 'winston';
import * as TransportStream from 'winston-transport';

/*
LOG LEVELS:
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
 */

const LOG_FOLDER = `logs`;
const DEFAULT_LOGGER = 'DEFAULT';
const DEFAULT_FORMAT = format.combine(
  format.timestamp({
    format: 'YYYY-MM-DD HH:mm:ss'
  }),
  format.errors({ stack: true }),
  // format.align(),
  format.splat(),
  format.printf((info: TransformableInfo): string => getLogLineFormatPrintf(info, true))
);

const winstonTransports: TransportStream[] = [];

switch (environment.environment) {
  case Environments.PROD:
    // if (!fs.existsSync(LOG_FOLDER)) {
    //   fs.mkdirSync(LOG_FOLDER);
    // }
    winstonTransports.push(new transports.Console({ level: 'error' }));
    // winstonTransports.push(new transports.File({ filename: 'error.log', level: 'error', dirname: LOG_FOLDER }));
    // winstonTransports.push(new transports.File({ filename: 'combined.log', dirname: LOG_FOLDER }));
    break;
  case Environments.DEV:
    winstonTransports.push(
      new winston.transports.Console({
        level: process.env.LOGGER_LEVEL || 'info',
        format: format.combine(
          DEFAULT_FORMAT,
          format.printf((info: TransformableInfo): string => getLogLineFormatPrintf(info, false))
        )
      })
    );
    break;
  case Environments.LOCAL:
  default:
    winstonTransports.push(
      new winston.transports.Console({
        level: process.env.LOGGER_LEVEL || 'info',
        format: format.combine(
          DEFAULT_FORMAT,
          format.colorize({ message: true, level: true }),
          format.printf((info: TransformableInfo): string => getLogLineFormatPrintf(info, false))
        )
      })
    );
    break;
}

function getLogLineFormatPrintf(
  { level, message, label, timestamp, process }: TransformableInfo,
  padding: boolean = false
): string {
  let processMessagePart = '';
  if (Boolean(process)) {
    processMessagePart = ` | ${process}`;
  }
  label += processMessagePart;

  if (padding) {
    label = label.padEnd(15);
    level = level.padEnd(7);
  }

  return `[${timestamp}] [${level}] [${label}]: ${message}`;
}

export function createLoggerConfig(category: string): winston.LoggerOptions {
  return {
    level: 'info',
    format: format.combine(
      format.label({
        label: category
      }),
      DEFAULT_FORMAT
    ),
    transports: winstonTransports
  };
}

export const defaultLogger = loggers.add(DEFAULT_LOGGER, createLoggerConfig(DEFAULT_LOGGER));
