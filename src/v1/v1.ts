import { RoutesProvider } from '@/routes-provider';
import { Recast } from '@client/cai/recast';

export class V1 extends RoutesProvider {
  protected mountRoutes(): void {
    this.router.use('/cai', new Recast().getRouter());
  }
}
