import { CAIUtils, CAIBody, caiLogger } from '../util/cai.util';
import { Request } from 'express';
import { Memoryfields } from '../model/memoryfields-enum';

export class Spotcoaching {
  constructor() { }

  /**
   * Webhook which a question for the given category.
   */
  public async getQuestion(req: Request): Promise<CAIBody> {
    const logger = caiLogger.child({ process: 'spotcoaching - getQuestion' });
    logger.info('Start');
    logger.silly('%o', { req });

    const body: CAIBody = CAIUtils.initBody(req);
    const categroy = body.conversation.memory[Memoryfields.coach_categoryKey] || null

    logger.info('found category', categroy)

    
    req.setLocale(req.body.nlp.language); // set language for messages
    body.replies.push({
      type: 'text',
      content: `gewählte kategorie: ${categroy}`
    });

    return CAIUtils.reply(body);
  }
}