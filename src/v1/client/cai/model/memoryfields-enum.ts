export enum Memoryfields {
    // general
    userIntent = 'userIntent',
    waitFreetext = 'waitFreetext',
    inputFreetext = 'inputFreetext',
    waitNumber = 'waitNumber',
    inputNumber = 'inputNumber',

    // spotcoaching
    coach_categoryKey = 'coach_categoryKey',
  
    // account
    acc_account = 'acc_account',
    acc_contract = 'acc_contract',
    acc_organisation = 'acc_organisation',
    acc_config = 'acc_config',
    // acc_integration = 'acc_integration', // FBI Disabled 20190701
  
    // auth
    auth_userid = 'auth_userid',
    auth_origin = 'auth_origin',
  
    // file
    file_type = 'file_type',
    file_name = 'file_name',
    file_apiResult = 'file_apiResult',
  
    // expense
    exp_reqfullfilled = 'exp_reqfullfilled',
    exp_cancel = 'exp_cancel',
    exp_getFreetext = 'exp_getFreetext',
    exp_getNumber = 'exp_getNumber',
    exp_optInput = 'exp_optInput',
    exp_location = 'exp_location',
    exp_city = 'exp_city',
    exp_countryCode = 'exp_countryCode',
    exp_type = 'exp_type',
    exp_typeKey = 'exp_typeKey',
    exp_group = 'exp_group',
    exp_category = 'exp_category',
    exp_categoryKey = 'exp_categoryKey',
    exp_amount = 'exp_amount',
    exp_currency = 'exp_currency',
    exp_currencyCode = 'exp_currencyCode',
    exp_paymentType = 'exp_paymentType',
    exp_report = 'exp_report',
    exp_vat = 'exp_vat',
    exp_vatClassificationKey = 'exp_vatClassificationKey',
    exp_vatNumber = 'exp_vatNumber',
    exp_transactionDate = 'exp_transactionDate',
    exp_merchant = 'exp_merchant',
    exp_language = 'exp_language',
  
    // absence
    abs_rep_function = 'abs_rep_function',
    abs_mec_confirmation = 'abs_mec_confirmation',
  
    // time
    tim_starttime = 'tim_starttime',
    tim_endtime = 'tim_starttime',
    tim_duration = 'tim_starttime',
    tim_duration_ext = 'tim_starttime'
  }
  