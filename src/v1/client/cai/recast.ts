import i18n from '@i18n/i18n.config';
import express, { Request, Response } from 'express';
import { Spotcoaching } from './m/spotcoaching';
import { System } from './system';
import { CAIUtils } from './util/cai.util';


export class Recast {
  private router: any;

  constructor() {
    this.router = express.Router();
    this.router.use(i18n.init);
    this.mountRoutes();
  }

  private mountRoutes(): void {
    this.router.get('/', (req: Request, res: Response): void => {
      CAIUtils.logInfo(req);
      CAIUtils.returnMessage(res, { message: 'spotcoaching cai api services' });
    });

    // *************************************************************
    // General
    // *************************************************************
    this.router.post('/version', (req: Request, res: Response): void => {
      new System().version(req).then((data: any): void => {
        CAIUtils.returnMessage(res, data);
      });
    });

    this.router.post('/fallback', (req: Request, res: Response): void => {
      new System().logFallback(req).then((data: any): void => {
        CAIUtils.returnMessage(res, data);
      });
    });

    this.router.post('/detectNumber', (req: Request, res: Response): void => {
      CAIUtils.detectNumber(req).then((data: any): void => {
        CAIUtils.returnMessage(res, data);
      });
    });

    // *************************************************************
    // Spotcoaching
    // *************************************************************
    this.router.post('/m/spotcoaching/getQuestion', (req: Request, res: Response): void => {
      new Spotcoaching().getQuestion(req).then((data: any): void => {
        CAIUtils.returnMessage(res, data);
      });
    });

  }

  public getRouter(): any {
    return this.router;
  }
}
