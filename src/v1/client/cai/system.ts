import { environment } from '@environment/environment';
import { Environments } from '@environment/environment-definition';
import { Request } from 'express';
import { CAIUtils, CAIBody, caiLogger } from './util/cai.util';

const gitVersion = require('../../../../git-version.json');

export class System {
  constructor() {}

  /**
   * Webhook which returns version-information.
   */
  public async version(req: Request): Promise<CAIBody> {
    const body = CAIUtils.initBody(req);
    req.setLocale(req.body.nlp.language); // set language for messages

    let version: string;

    if (environment.environment === Environments.PROD) {
      version = gitVersion.tag;
    } else {
      version = gitVersion.semverString;
    }

    body.replies.push({
      type: 'text',
      content: `Environment: ${environment.environment.toString()} | Version: ${version}`
    });

    return CAIUtils.reply(body);
  }

  /**
   * Webhook which writes fallback-information to database.
   */
  public async logFallback(req: Request): Promise<CAIBody> {
    const logger = caiLogger.child({ process: 'system - logFallback' });
    logger.info('cai fallback triggered', req.body);
    const body = CAIUtils.initBody(req);
    
    return CAIUtils.reply(body);
  }
}
