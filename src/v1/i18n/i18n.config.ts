import * as i18n from 'i18n';

i18n.configure({
  defaultLocale: 'de',
  locales: ['de', 'en', 'fr'],
  directory: './src/v1/i18n/locales',
  objectNotation: true
});

export default i18n;
