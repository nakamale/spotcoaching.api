import CleanWebpackPlugin from 'clean-webpack-plugin';
import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';
import nodeExternals from 'webpack-node-externals';
import common from './webpack.common';


const configDev: webpack.Configuration = {
  devtool: 'inline-source-map',
  entry: ['webpack/hot/poll?1000', path.join(__dirname, 'src/main.ts')],
  externals: [
    nodeExternals({
      whitelist: ['webpack/hot/poll?1000']
    })
  ],
  mode: 'development',
  plugins: [
    new CleanWebpackPlugin({ cleanAfterEveryBuildPatterns: ['!assets/**/*'] }),
    new webpack.HotModuleReplacementPlugin()
  ],
  watch: true
};

const config = merge.smart(common, configDev);
export default config;
